from rest_framework import serializers

class IssueSerializer(serializers.Serializer):
    subject = serializers.CharField()
    id = serializers.IntegerField(read_only=True)
    # extra_kwargs = {'id': {'write_only': True}}

    def create(self, instance):
        return instance
