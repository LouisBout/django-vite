from rest_framework.generics import ListCreateAPIView
from .serializers import IssueSerializer


class IssueList(ListCreateAPIView):
    serializer_class = IssueSerializer

    def get_queryset(self):
        return [{"subject": "ptere", "id": 54654}]

    def perform_create(self, serializer):
        serializer.save(id=155)