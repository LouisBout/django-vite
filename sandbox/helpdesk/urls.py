from .api_views import IssueList
from django.urls import path

app_name = 'helpdesk_api'

urlpatterns = [
    path('issues/', view=IssueList.as_view(), name="issue_list_create")
]