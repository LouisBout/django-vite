module.exports = {
  mode: 'jit',
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'ecritel': {
          DEFAULT: '#70BC14',
          dark: '#4d586d'
        }
      },
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
