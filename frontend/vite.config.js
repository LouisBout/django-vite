import vue from '@vitejs/plugin-vue'
import { defineConfig } from 'vite'

const { resolve } = require('path')

const isProtocolSecure = process.env.FRONTEND_DEV_SERVER_PROTOCOL === 'https'

// https://vitejs.dev/config/
export default defineConfig({
  root: resolve('./src'),
  base: '/static/',
  server: {
    host: '0.0.0.0',
    port: 3000,
    open: false,
    hmr: {
      host: process.env.FRONTEND_URL,
      port: isProtocolSecure ? 443 : 80,
      protocol: isProtocolSecure ? 'wss' : 'ws'
    },
    watch: {
      usePolling: true,
      disableGlobbing: false,
    }
  },
  build: {
    outDir: resolve('./dist'),
    assetsDir: '',
    emptyOutDir: true,
    manifest: true,
    rollupOptions: {
      input: {
        main: resolve('./src/main.js')
      }
    }
  },
  css: {
    preprocessorOptions: {
      less: { 
         // example : additionalData: `@import "./src/design/styles/variables";`
         // dont need include file extend .scss
         additionalData: `@import "./src/assets/variables.less";` 
     },
    },
  },
  plugins: [vue()]
})
